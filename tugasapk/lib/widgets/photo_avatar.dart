import 'package:flutter/material.dart';

class PhotoAvatar extends StatelessWidget {
  const PhotoAvatar({super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(999),
        child: Image.asset(
          'assets/profile.png',
          width: 150,
          height: 150,
          fit: BoxFit.cover,
        ));
  }
}
