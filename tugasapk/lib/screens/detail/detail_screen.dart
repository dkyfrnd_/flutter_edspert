import 'package:flutter/material.dart';
import 'package:tugasapk2/widgets/photo_avatar.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Details'),
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: SizedBox(
            width: double.infinity,
            child: ListView(
              children: [
                Column(
                  children: [
                    PhotoAvatar(),
                    const SizedBox(
                      height: 16,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // PhotoAvatar(),
                        Text('Nama\t\t: Dicky Fernando Sitepu'),
                        Text('Pengalaman : '),
                        Padding(
                          padding: const EdgeInsets.only(left: 16),
                          child: Column(
                            children: [
                              Text('- Game Developer'),
                              Text('- Software Enginer'),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Go to home'),
                    )
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
