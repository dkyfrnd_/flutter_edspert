import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:tugasapk2/screens/detail/detail_screen.dart';
import 'package:tugasapk2/widgets/photo_avatar.dart';




class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          color: Colors.blueAccent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 16,),
              PhotoAvatar(),
              SizedBox(height: 16,),
              Text('Dicky Fernando Sitepu', 
                style: TextStyle(
                color: Colors.white,
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
              ),
              SizedBox(height: 40,),
              ElevatedButton(
                onPressed: (){
                  Navigator.push(
                    context,  
                    MaterialPageRoute(
                      builder: (context) {
                        return DetailScreen();
                      },
                    ), 
                  );
                }, 
                child: Text('Go to detail')
              ),
            ],
            
          ),
        ),
      ),
    );
  }
}
